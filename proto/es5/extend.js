// 重写原型链
function Supertype() {
    this.property = true;
}
Supertype.prototype.getSuperValue = function() {
    console.log(this.property)
}

function SubType() {
    this.subproperty = false;
}

SubType.prototype = new Supertype();

SubType.prototype.getSubValue = function () {
    console.log(this.subproperty)
}

var sub = new SubType();
console.log(sub.getSuperValue());

//使用构造函数
function Person(name, age) {
     this.colors = ['red', 'blue', 'green']
 }

 function Person1 (name, age)  {
    console.log(this)
    Person.call(this, name, age);
    
 }
 var s1 = new Person1('Bob', 18);
 var s2 = new Person1('Mick', 20);
 s2.colors.push('black');

 console.log(s1.colors)
 console.log(s2.colors)

 //组合继承
function Man (name){
    this.name =name
    this.age = 28
}
Man.prototype.sayName = function () {
    return this.name;
}
function Team(name, age) {
     Man.call(this, name)
}
Team.prototype = new Man()
Team.prototype.constructor = Team;
Team.prototype.sayName = function(){
    console.log(this.age)
}


//组合

function Person(name, age) {
    this.name = name
    this.age =age
}
Person.prototype.sayName = function() {
    console.log(this.age)
}
function Man(name, age) {
      Person.call(this, name, age)
      this.color = ["pink", "red"]
}

Man.prototype = new Person();
Man.prototype.constructor = Man;
Man.prototype.sayHello= function (){
    console.log("hello")
}

//寄生组合
