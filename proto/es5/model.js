// 工厂模式   缺点：无法知道对象的类型
function Person1(name, age) {
    var o = new Object();
    o.name = name;
    o.age = age
    return o
}

//构造函数 
function Person2(name, age) {
    this.name = name;
    this.age = age;
    this.sayName = function() {
        console.log(this.name)
    }
}

function Person3(name, age) {
    this.name = name;
    this.age = age;
    this.sayName = sayName;
}
function sayName(){
    console.log(this.name);
}

// 原型模式
function Person4() {  
}
Person4.prototype.name = "lance";
Person4.prototype.sayName = () => {
    console.log(this.name);
}

//组合使用构造函数和原型模式
function Person5(name, age) { 
    this.name = name
    this.age = age
    this.lesson = ['js', 'ts', 'go']
 }
 Person5.prototype.sayName = function() {
     console.log(this.name)
 }

 // 动态原型模式
function Person6(name, age) {
    this.name = name;
    this.age = age;

    // 只需要检查一个方法即可
    if (typeof this.sayName !== 'function') {
        Person.prototype.sayName = function () {
            return this.name;
        }
        Person.prototype.sayAge = function () {
            return this.age;
        }
    }
}


