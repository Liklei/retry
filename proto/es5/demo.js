/**
 * 对象中的原型
 **/

function Person() {}
// 创建函数时，就会为函数创建一个prototype属性，指向原型对象
console.log(Person.prototype)
//原型对象会会获得一个constructor属性，指向prototype所在函数的指针
console.log(Person.prototype.constructor)
console.log(Person.prototype.__proto__)

//每个构造函数都有一个原型对象Person.prototype,原型对象都包含一个指向构造函数的指针，
//实例内部包含一个指向原型对象的指针__proto__
var worker = new Person()
console.log(worker.__proto__)

//每个对象都有__proto__,只有函数才有prototype属性

// 在创建原型链时，如果直接重写原型链，相当于切断原型与构造函数之间的关系，此时prototype.constructor指向
// Object而不是cars
function Cars() {}
Cars.prototype = {
    name: 'Cars',
    sayName: function () {
        return this.name;
    }
}

function Animal() {}
Animal.prototype = {
    constructor: Animal,
    name: 'Animal',
    sayName: function () {
        return this.name;
    }
}
var c = new Cars();
var a = new Animal();

console.log(c.__proto__.constructor)
console.log(a.__proto__.constructor)

console.log(c.sayName())
console.log(a.sayName())





